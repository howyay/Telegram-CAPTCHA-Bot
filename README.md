# Telegram-CAPTCHA-Bot

一个用于验证新加入群员是否为真人的 Bot 。

原项目：https://github.com/lziad/Telegram-CAPTCHA-bot

本项目由 [@TH779](https://t.me/TH779) 修改并发布。

## 现有实例

[@YingLin's CAPTCHA Bot](https://t.me/YingLin_CAPTCHABot)

## 安装、配置及使用方法

1. 向 [@BotFather](https://t.me/BotFather) 申请一个 Bot ，并将 Bot API Token 复制。
2. 在服务器上安装 Python-Telegram-Bot ： `pip3 install --upgrade python-telegram-bot` 。
3. Clone 并打开本项目：`git clone https://gitlab.com/hh2333/Telegram-CAPTCHA-Bot.git && cd Telegram-CAPTCHA-Bot` 。
4. 将项目下 config.json 里的 token 字符串修改为第一步所获取到的 Bot API Token ，除此之外你也可以修改里面的配置选项。
5. 使用 `python3 main.py` 运行此 Bot 。

## 开源协议

本项目使用 MIT 许可协议发布。
